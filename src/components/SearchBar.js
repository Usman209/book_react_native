import React from "react";
import { View, Text,TextInput ,StyleSheet } from "react-native";
import { Feather } from '@expo/vector-icons'

const SearchBar = ({term,onTermChange,onTermSubmit}) => {
  return (
    <View style={ styles.background }>
      <Feather name="search" style={styles.iconStyle} />
      <TextInput style={ styles.inputStyle }
        autoCapitalize='none'
        autoCorrect={ false }
        placeholder="Search"
        value={ term }
        onChangeText={ onTermChange }
        onEndEditing={ onTermSubmit }
      />
    </View>
  );
};

const styles = StyleSheet.create( {
  
  background: {
    backgroundColor: 'grey',
    marginTop:10,
    height: 50,
    borderRadius: 5,
    marginHorizontal: 15,
    flexDirection: 'row',
    marginBottom:10
    
  },
  inputStyle: {
    fontSize:18,
    flex:1
  },
  iconStyle: {
    fontSize: 35,
    alignSelf: 'center',
    marginHorizontal:15
  }

});

export default SearchBar;
