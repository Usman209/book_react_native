import React, { useState, useEffect } from "react";
import yelp from "../api/yelp";



export default () =>
{
  const [results, setResults] = useState([]);
  const [errorMessage, setErrorMessage] = useState("");

  const searchApi = async searchTerm  => {
    try {
      const response = await yelp.get("/books", {
       
      });
      setResults(response.data.data.data);
    } catch (err) {
      setErrorMessage("Something Went Wrong");
    }
  };

  useEffect(() => {
    searchApi('pasta');
  }, []);

  return [searchApi,results,errorMessage]
}
